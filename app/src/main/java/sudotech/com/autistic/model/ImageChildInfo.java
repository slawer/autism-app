package sudotech.com.autistic.model;

public class ImageChildInfo {
    String  imageChildDesc;
    int conColor;

    public ImageChildInfo(String imageChildDesc, int conColor) {
        this.imageChildDesc = imageChildDesc;
        this.conColor = conColor;
    }
    

    public String getImageChildDesc() {
        return imageChildDesc;
    }

    public void setImageChildDesc(String imageChildDesc) {
        this.imageChildDesc = imageChildDesc;
    }

    public int getConColor() {
        return conColor;
    }

    public void setConColor(int conColor) {
        this.conColor = conColor;
    }
}
