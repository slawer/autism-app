package sudotech.com.autistic.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import sudotech.com.autistic.R;
import sudotech.com.autistic.adapter.MessageAdapter;
import sudotech.com.autistic.adapter.PictureAdapter;
import sudotech.com.autistic.model.ImageChildInfo;
import sudotech.com.autistic.model.ImageParent;
import sudotech.com.autistic.model.Messages;
import sudotech.com.autistic.util.GetTimeAgo;

public class ChatActivity extends AppCompatActivity {

    private String mChatUser;
    private Toolbar mChatToolbar;

    private DatabaseReference mRootRef;

    private TextView mTitleView;
    private TextView mLastSeenView;
    private CircleImageView mProfileImage;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private ImageButton mChatAddBtn;
    private ImageButton mChatSendBtn;
    private EditText mChatMessageView;

    private RecyclerView mMessagesList;


    private final List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapter mAdapter;

    private static final int TOTAL_ITEMS_TO_LOAD = 10;
    private int mCurrentPage = 1;

    private static final int GALLERY_PICK = 1;

    // Storage Firebase
    private StorageReference mImageStorage;


    private int itemPos = 0;

    private String mLastKey = "";
    private String mPrevKey = "";

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    String user_type = "";
    String userName;

    static TextToSpeech textToSpeech;

    BottomSheetBehavior bottomSheetBehavior;

    private List<ImageParent> imageParentList;
    PictureAdapter pictureAdapter;
    RecyclerView pictureRecycler;

    boolean clicked = false;

    public static EditText edtMsg;
    public FloatingActionButton fabSend;
    public Button btnClear;
    public ImageView imgDownBtn;

    public static LinearLayout linearLayout;

    String message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        prefs = PreferenceManager.getDefaultSharedPreferences(ChatActivity.this);
        edit = prefs.edit();

        imageParentList = new ArrayList<>();

        mChatToolbar = findViewById(R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);

        View bottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);


        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setPeekHeight(0);

        edtMsg = bottomSheet.findViewById(R.id.edtMsg);
        fabSend = bottomSheet.findViewById(R.id.fabSend);
        btnClear = bottomSheet.findViewById(R.id.btnClear);
        imgDownBtn = bottomSheet.findViewById(R.id.imgDownBtn);


        linearLayout = bottomSheet.findViewById(R.id.linearSend);


        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();

        mChatUser = getIntent().getStringExtra("user_id");
        userName = getIntent().getStringExtra("user_name");

        user_type = prefs.getString("usertype", "");

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);

        actionBar.setCustomView(action_bar_view);


        pictureRecycler = findViewById(R.id.pictureRecycler);

        pictureRecycler.setHasFixedSize(true);
        GridLayoutManager llm = new GridLayoutManager(ChatActivity.this, 3);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        //llm.setSpanCount(4);
        pictureRecycler.setLayoutManager(llm);

        initData();


        // ---- Custom Action bar Items ----

        mTitleView = findViewById(R.id.custom_bar_title);
        mLastSeenView = findViewById(R.id.custom_bar_seen);
        mProfileImage = findViewById(R.id.custom_bar_image);

        mChatAddBtn = findViewById(R.id.chat_add_btn);
        mChatSendBtn = findViewById(R.id.chat_send_btn);
        mChatMessageView = findViewById(R.id.chat_message_view);

        mAdapter = new MessageAdapter(messagesList, this);

        mMessagesList = findViewById(R.id.messages_list);
        mLinearLayout = new LinearLayoutManager(this);

        mMessagesList.setHasFixedSize(true);
        mMessagesList.setLayoutManager(mLinearLayout);

        mMessagesList.setAdapter(mAdapter);

        //------- IMAGE STORAGE ---------
        mImageStorage = FirebaseStorage.getInstance().getReference();

        mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);

        loadMessages();


        mTitleView.setText(userName);

        mRootRef.child("Users").child(mChatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String online = dataSnapshot.child("online").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();

                if (online.equals("true")) {

                    mLastSeenView.setText("Online");

                } else {

                    GetTimeAgo getTimeAgo = new GetTimeAgo();

                    long lastTime = Long.parseLong(online);

                    String lastSeenTime = GetTimeAgo.getTimeAgo(lastTime, getApplicationContext());

                    mLastSeenView.setText(lastSeenTime);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mRootRef.child("Chat").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.hasChild(mChatUser)) {

                    Map chatAddMap = new HashMap();
                    chatAddMap.put("seen", false);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap = new HashMap();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                    chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                    mRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if (databaseError != null) {

                                Log.d("CHAT_LOG", databaseError.getMessage().toString());

                            }

                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String intent_message = extras.getString("pic_message");

            sendPicMessage(intent_message);
        }

        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendMessage();

            }
        });

        if (user_type.equals("autistic")) {

            mChatMessageView.setVisibility(View.GONE);
            mChatSendBtn.setVisibility(View.GONE);
            mChatAddBtn.setVisibility(View.VISIBLE);
        } else if (user_type.equals("parent")) {
            mChatMessageView.setVisibility(View.VISIBLE);
            mChatSendBtn.setVisibility(View.VISIBLE);
            mChatAddBtn.setVisibility(View.GONE);
//                    Toast.makeText(ChatActivity.this, "You can only send text", Toast.LENGTH_SHORT).show();
        }

        mChatAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (user_type.equals("autistic")) {

                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                }
            }
        });

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = textToSpeech.setLanguage(Locale.ENGLISH);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(ChatActivity.this, "This language is not supported", Toast.LENGTH_SHORT).show();
                    } else {
                        textToSpeech.setPitch(0.6f);
                        textToSpeech.setSpeechRate(1.0f);
                        speak();
                    }
                }
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtMsg.setText("");
                linearLayout.setVisibility(View.GONE);
            }
        });
        fabSend.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           sendPicMessage(edtMsg.getText().toString());
                                       }
                                   }
        );

        imgDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

    }

    private void initData() {

        //Actions
        ArrayList<ImageChildInfo> infoArrayList = new ArrayList<ImageChildInfo>();
        ImageChildInfo imageChildInfo = new ImageChildInfo("Ask", R.drawable.ask);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Bathe", R.drawable.bathe);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Body Shake", R.drawable.body_shakes);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Bring", R.drawable.bring);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Buy", R.drawable.buy);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Carry", R.drawable.carry);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Change Clothes", R.drawable.change_clothes);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Check Message", R.drawable.check_messages);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Choke", R.drawable.choke);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Clap", R.drawable.clap);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Come", R.drawable.come);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Congratulations", R.drawable.congratulations);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Dance", R.drawable.dance);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Don't", R.drawable.dont);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Dress Up", R.drawable.dress_up);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Drink", R.drawable.drink);
        infoArrayList.add(imageChildInfo);
        imageParentList.add(new ImageParent("actions", infoArrayList));


        //Body Parts
        infoArrayList = new ArrayList<ImageChildInfo>();
        imageChildInfo = new ImageChildInfo("Ankle", R.drawable.ankle);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Arm", R.drawable.arm);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Back", R.drawable.back);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Buttocks", R.drawable.buttocks);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Chest", R.drawable.chest);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Chin", R.drawable.chin);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Ear", R.drawable.ear);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Elbow", R.drawable.elbow);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Eye", R.drawable.eye);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Feet", R.drawable.feet);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Hand", R.drawable.hand);
        infoArrayList.add(imageChildInfo);
        imageParentList.add(new ImageParent("body parts", infoArrayList));


        //Clothing
        infoArrayList = new ArrayList<ImageChildInfo>();
        imageChildInfo = new ImageChildInfo("Belt", R.drawable.belt);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Blouse", R.drawable.blouse);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Boxers", R.drawable.boxers);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Boys Slipper", R.drawable.boys_slipper);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Bra", R.drawable.bra);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Bracelet", R.drawable.bracelet);
        infoArrayList.add(imageChildInfo);
        imageChildInfo = new ImageChildInfo("Briefs", R.drawable.briefs);
        infoArrayList.add(imageChildInfo);
        imageParentList.add(new ImageParent("clothing", infoArrayList));


        pictureAdapter = new PictureAdapter(imageParentList, this);

        pictureRecycler.setAdapter(pictureAdapter);


    }

    private void speak() {
        String sas = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(sas, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            textToSpeech.speak(sas, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    protected void onDestroy() {
        //Shutdown t2s when app closes
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();

            final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            final String push_id = user_message_push.getKey();


            StorageReference filepath = mImageStorage.child("message_images").child(push_id + ".jpg");

            filepath.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if (task.isSuccessful()) {

                        String download_url = task.getResult().getDownloadUrl().toString();


                        Map messageMap = new HashMap();
                        messageMap.put("message", download_url);
                        messageMap.put("seen", false);
                        messageMap.put("type", "image");
                        messageMap.put("time", ServerValue.TIMESTAMP);
                        messageMap.put("from", mCurrentUserId);

                        Map messageUserMap = new HashMap();
                        messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                        messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                        mChatMessageView.setText("");

                        mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if (databaseError != null) {

                                    Log.d("CHAT_LOG", databaseError.getMessage().toString());

                                }

                            }
                        });


                    }

                }
            });

        }

    }


    private void loadMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser);

        Query messageQuery = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);


        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Messages message = dataSnapshot.getValue(Messages.class);

                //itemPos++;

                //if (itemPos == 1) {

                String messageKey = dataSnapshot.getKey();

                mLastKey = messageKey;
                mPrevKey = messageKey;

                //}

                messagesList.add(message);
                mAdapter.notifyDataSetChanged();

                mMessagesList.scrollToPosition(messagesList.size() - 1);

                //mRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage() {


        String message = mChatMessageView.getText().toString();

        if (!TextUtils.isEmpty(message)) {

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();

            DatabaseReference newNotificationref = mRootRef.child("msgNotifications").child(mChatUser).push();
            String newNotificationId = newNotificationref.getKey();

            HashMap<String, String> notificationData = new HashMap<>();
            notificationData.put("from", mCurrentUserId);
            notificationData.put("type", "message");


            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);
            //messageMap.put("msgNotifications/" + mCurrentUserId + "/" + newNotificationId, notificationData);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mChatMessageView.setText("");

            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);

            Map requestMap = new HashMap();
            requestMap.put("msgNotifications/" + mChatUser + "/" + newNotificationId, notificationData);

            mRootRef.updateChildren(requestMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Toast.makeText(ChatActivity.this, "There was some error in sending request", Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(ChatActivity.this, "Message Sent", Toast.LENGTH_SHORT).show();
//                        mCurrent_state = "req_sent";
//                        mProfileSendReqBtn.setText("Cancel Friend Request");

                    }

                   // mProfileSendReqBtn.setEnabled(true);


                }
            });

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if (databaseError != null) {

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });

        }

    }

    public static void speak(String aa) {
        //String text = edtMsg.getText().toString();

        if (!aa.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textToSpeech.speak(aa, TextToSpeech.QUEUE_FLUSH, null, null);
                edtMsg.append(" " + aa.toLowerCase());
            } else {
                textToSpeech.speak(aa, TextToSpeech.QUEUE_FLUSH, null);
            }
        } else {

        }
    }


    private void sendPicMessage(String message) {


        //String message = mChatMessageView.getText().toString();

        if (!TextUtils.isEmpty(message)) {

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            edtMsg.setText("");
            linearLayout.setVisibility(View.GONE);

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mChatMessageView.setText("");

            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if (databaseError != null) {

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });

        }

    }
}










































