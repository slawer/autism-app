package sudotech.com.autistic.viewholder;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import sudotech.com.autistic.R;


public class ImageViewHolder extends GroupViewHolder {

    private TextView jobTitle;
    private TextView side_color;

    public ImageViewHolder(View itemView) {
        super(itemView);

        jobTitle = itemView.findViewById(R.id.job_title);
        side_color = itemView.findViewById(R.id.side_color);
    }

     public void setJobTitle(String name){
        jobTitle.setText(name);
     }

    public void setSideColor(int color) {
        side_color.setBackgroundColor(color);
    }
}
