package sudotech.com.autistic.viewholder;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import sudotech.com.autistic.R;


public class ImageChildViewHolder extends ChildViewHolder {

    private TextView jobDesc;
    private TextView side_color;
    private ImageView img;
    public CardView cardView;

    public ImageChildViewHolder(View itemView) {
        super(itemView);

        jobDesc=itemView.findViewById(R.id.job_desc);
        img=itemView.findViewById(R.id.img);
        side_color=itemView.findViewById(R.id.side_color);
        cardView=itemView.findViewById(R.id.cardview1);
    }

    public void setJobDesc(String name) {
        jobDesc.setText(name);
    }

//    public void setImg(String name) {
//        img.setImageResource(Integer.parseInt(name));
//    }

    public void setConColor(int conColor) {
        img.setImageResource(conColor);
    }

    public void setSideColor(int color) {
        side_color.setBackgroundColor(color);
    }
}
