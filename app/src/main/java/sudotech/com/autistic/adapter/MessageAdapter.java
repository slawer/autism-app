package sudotech.com.autistic.adapter;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import sudotech.com.autistic.model.Messages;
import sudotech.com.autistic.R;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {


    private List<Messages> mMessageList;
    private DatabaseReference mUserDatabase;
    public Context context;

    public MessageAdapter(List<Messages> mMessageList, Context context) {

        this.mMessageList = mMessageList;
        this.context = context;

    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout, parent, false);

        return new MessageViewHolder(v);

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
        public CircleImageView profileImage;
        public TextView displayName;
        public TextView time_text_layout;
        public ImageView messageImage;
        public ImageButton imgbSpeak;
        public CardView cardView;


        public TextToSpeech textToSpeech;


        public MessageViewHolder(View view) {
            super(view);

            messageText = (TextView) view.findViewById(R.id.message_text_layout);
            time_text_layout = (TextView) view.findViewById(R.id.time_text_layout);
            profileImage = (CircleImageView) view.findViewById(R.id.message_profile_layout);
            displayName = (TextView) view.findViewById(R.id.name_text_layout);
            messageImage = (ImageView) view.findViewById(R.id.message_image_layout);
            imgbSpeak = (ImageButton) view.findViewById(R.id.imgbSpeak);
            cardView = view.findViewById(R.id.cardview1);


            textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        int result = textToSpeech.setLanguage(Locale.ENGLISH);
                        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Toast.makeText(context, "This language is not supported", Toast.LENGTH_SHORT).show();
                        } else {
                            textToSpeech.setPitch(0.6f);
                            textToSpeech.setSpeechRate(1.0f);
                            //speak();
                        }
                    }
                }
            });

        }

        public void speak() {
            //String text = edtMsg.getText().toString();


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textToSpeech.speak(messageText.getText().toString(), TextToSpeech.QUEUE_FLUSH, null, null);
            } else {
                textToSpeech.speak(messageText.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {

        Messages c = mMessageList.get(i);

        String from_user = c.getFrom();
        String message_type = c.getType();
        long epoch = c.getTime();

        String date = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date(epoch));


        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(from_user);

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue().toString();
                String image = dataSnapshot.child("thumb_image").getValue().toString();

                viewHolder.displayName.setText(name);

                Picasso.with(viewHolder.profileImage.getContext()).load(image)
                        .placeholder(R.drawable.default_avatar).into(viewHolder.profileImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (message_type.equals("text")) {

            if(from_user.equals(c.getFrom())) {
                viewHolder.messageText.setText(c.getMessage());
                viewHolder.time_text_layout.setText(date);
                //viewHolder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorGray));
                viewHolder.messageImage.setVisibility(View.GONE);


                viewHolder.imgbSpeak.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewHolder.speak();
//                    Toast.makeText(context, viewHolder.messageText.getText(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(context, viewHolder.messageText.getText(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }

        else if (message_type.equals("text")) {

            if(!from_user.equals(c.getFrom())) {
                viewHolder.messageText.setText(c.getMessage());
                viewHolder.time_text_layout.setText(date);
                viewHolder.messageImage.setVisibility(View.GONE);


                viewHolder.imgbSpeak.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewHolder.speak();
//                    Toast.makeText(context, viewHolder.messageText.getText(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(context, viewHolder.messageText.getText(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
        else {

            viewHolder.messageText.setVisibility(View.GONE);
            Picasso.with(viewHolder.profileImage.getContext()).load(c.getMessage())
                    .placeholder(R.drawable.default_avatar).into(viewHolder.messageImage);


        }

    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


}
