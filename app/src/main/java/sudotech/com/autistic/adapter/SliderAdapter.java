package sudotech.com.autistic.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sudotech.com.autistic.R;


/**
 * Created by nigma on 15/03/2018.
 */

public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_image={
            R.drawable.create_account,
            R.drawable.add_friends,
            R.drawable.use_symbols,
            R.drawable.chat
    };

    public String[] slide_headings={
            "Create account",
            "Add friends",
            "Use PECS Symbols",
            "Chat"
    };

    public String[] slide_descrip={
            "Set-up your profile and start communicating with friends!\n",
            "Send requests to other users and make friends!",
            "Select from a wide range of symbols. Group them together \n" +
                    "to form  full sentences!",
            "Start conversations with friends and family!"
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view== object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageView = view.findViewById(R.id.slide_image);
        TextView slideHeader = view.findViewById(R.id.slide_header);
        TextView slideDescription = view.findViewById(R.id.slide_description);

        slideImageView.setImageResource(slide_image[position]);
        slideHeader.setText(slide_headings[position]);
        slideDescription.setText(slide_descrip[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout)object);

    }
}
