package sudotech.com.autistic.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import sudotech.com.autistic.model.ImageChildInfo;
import sudotech.com.autistic.R;
import sudotech.com.autistic.viewholder.ImageChildViewHolder;
import sudotech.com.autistic.viewholder.ImageViewHolder;

import static sudotech.com.autistic.activity.ChatActivity.linearLayout;
import static sudotech.com.autistic.activity.ChatActivity.speak;


public class PictureAdapter extends ExpandableRecyclerViewAdapter<ImageViewHolder, ImageChildViewHolder> {
    Context context;

    public PictureAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.context = context;
    }

    @Override
    public ImageViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_cardview, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public ImageChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_child_cardview, parent, false);
        return new ImageChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(ImageChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final ImageChildInfo imageChildInfo = (ImageChildInfo) group.getItems().get(childIndex);

        holder.setJobDesc(imageChildInfo.getImageChildDesc());
        holder.setConColor(imageChildInfo.getConColor());
        holder.setSideColor(imageChildInfo.getConColor());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, ""+imageChildInfo.getJobDesc(), Toast.LENGTH_SHORT).show();
                linearLayout.setVisibility(View.VISIBLE);
                speak(imageChildInfo.getImageChildDesc());
            }
        });
    }


    @Override
    public void onBindGroupViewHolder(ImageViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setJobTitle(group.getTitle());
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}

